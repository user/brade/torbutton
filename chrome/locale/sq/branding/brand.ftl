# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Shfletuesi Tor
-brand-short-name = Shfletuesi Tor
-brand-full-name = Shfletuesi Tor
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Shfletuesi Tor
-vendor-short-name = Projekti Tor
trademarkInfo = 'Tor' dhe 'Onion Logo' janë shenja tregtare të regjistruara të Tor Project, Inc.
