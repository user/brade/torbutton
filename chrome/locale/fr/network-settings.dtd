<!ENTITY torsettings.dialog.title "Paramètres du réseau Tor">
<!ENTITY torsettings.wizard.title.default "Se connecter à Tor">
<!ENTITY torsettings.wizard.title.configure "Paramètres du réseau Tor">
<!ENTITY torsettings.wizard.title.connecting "Établissement d’une connexion">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Langue du Navigateur Tor">
<!ENTITY torlauncher.localePicker.prompt "Veuillez choisir une langue.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Cliquez sur « Se connecter » pour vous connecter à Tor.">
<!ENTITY torSettings.configurePrompt "Cliquez sur « Configurer » pour régler les paramètres du réseau si vous êtes situé dans un pays qui censure Tor (tel que l’Égypte, la Chine, la Turquie) ou si vous vous connectez à partir d’un réseau privé qui exige un mandataire.">
<!ENTITY torSettings.configure "Configurer">
<!ENTITY torSettings.connect "Se connecter">

<!-- Other: -->

<!ENTITY torsettings.startingTor "En attente du démarrage de Tor…">
<!ENTITY torsettings.restartTor "Redémarrer Tor">
<!ENTITY torsettings.reconfigTor "Reconfigurer">

<!ENTITY torsettings.discardSettings.prompt "Vous avez configuré des ponts Tor ou vous avez saisi des paramètres de mandataire local.&#160;Pour établir une connexion directe vers le réseau Tor, ces paramètres doivent être supprimés.">
<!ENTITY torsettings.discardSettings.proceed "Supprimer les paramètres et se connecter">

<!ENTITY torsettings.optional "facultatif">

<!ENTITY torsettings.useProxy.checkbox "J’utilise un mandataire pour accéder à Internet">
<!ENTITY torsettings.useProxy.type "Type de mandataire">
<!ENTITY torsettings.useProxy.type.placeholder "sélectionner un type de mandataire">
<!ENTITY torsettings.useProxy.address "Adresse">
<!ENTITY torsettings.useProxy.address.placeholder "adresse IP ou nom d’hôte">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Nom d’utilisateur">
<!ENTITY torsettings.useProxy.password "Mot de passe">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Cet ordinateur passe par un pare-feu qui n’autorise que les connexions à certains ports">
<!ENTITY torsettings.firewall.allowedPorts "Ports autorisés">
<!ENTITY torsettings.useBridges.checkbox "Tor est censuré dans mon pays">
<!ENTITY torsettings.useBridges.default "Sélectionner un pont intégré">
<!ENTITY torsettings.useBridges.default.placeholder "Sélectionner un pont">
<!ENTITY torsettings.useBridges.bridgeDB "Demander un pont à torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Saisissez les caractères de l’image">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Obtenir un nouveau test">
<!ENTITY torsettings.useBridges.captchaSubmit "Envoyer">
<!ENTITY torsettings.useBridges.custom "Indiquer un pont que je connais">
<!ENTITY torsettings.useBridges.label "Saisir des renseignements de pont provenant d’une source fiable.">
<!ENTITY torsettings.useBridges.placeholder "type adresse:port (un par ligne)">

<!ENTITY torsettings.copyLog "Copier le journal de Tor dans le presse-papiers">

<!ENTITY torsettings.proxyHelpTitle "Aide sur les mandataires">
<!ENTITY torsettings.proxyHelp1 "Un mandataire local peut être nécessaire lors d’une connexion par un réseau d’entreprise, d’école ou d’université.&#160;Si vous n’êtes pas certain si un mandataire est nécessaire, vérifiez les paramètres Internet d’un autre navigateur ou les paramètres réseau de votre système.">

<!ENTITY torsettings.bridgeHelpTitle "Aide sur les relais-ponts">
<!ENTITY torsettings.bridgeHelp1 "Les ponts sont des relais non référencés qui rendent les connexions au réseau Tor plus difficiles à bloquer.&#160; Chaque type de pont utilise un moyen différent pour éviter la censure.&#160; Avec les ponts obfs, votre trafic ressemble à du bruit aléatoire, et avec les ponts meek, votre trafic semble se connecter à ce service plutôt qu’à Tor.">
<!ENTITY torsettings.bridgeHelp2 "Dans la mesure où certains pays tentent de bloquer Tor, certains ponts fonctionnent dans certains pays, mais pas dans d’autres.&#160;Si vous ne savez pas quels ponts fonctionnent dans votre pays, visitez torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Veuillez patienter pendant que nous établissons une connexion vers le réseau Tor.&#160;Cela pourrait prendre plusieurs minutes.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Connexion">
<!ENTITY torPreferences.torSettings "Paramètres de Tor">
<!ENTITY torPreferences.torSettingsDescription "Le Navigateur Tor achemine votre trafic par le réseau Tor, exploité par des milliers de bénévoles partout dans le monde.
" >
<!ENTITY torPreferences.learnMore "En apprendre davantage">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet :">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "En ligne">
<!ENTITY torPreferences.statusInternetOffline "Hors ligne">
<!ENTITY torPreferences.statusTorLabel "Réseau Tor :">
<!ENTITY torPreferences.statusTorConnected "Connecté">
<!ENTITY torPreferences.statusTorNotConnected "Non Connecté">
<!ENTITY torPreferences.statusTorBlocked "Possiblement bloqué">
<!ENTITY torPreferences.learnMore "En apprendre davantage">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Démarrage rapide">
<!ENTITY torPreferences.quickstartDescriptionLong "Lors du lancement, le démarrage rapide connecte automatiquement le Navigateur Tor au réseau Tor d’après les dernières options de connexion que vous avez utilisées.">
<!ENTITY torPreferences.quickstartCheckbox "Toujours se connecter automatiquement">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Ponts">
<!ENTITY torPreferences.bridgesDescription "Les ponts vous aident à accéder au réseau Tor où il est bloqué. 
Selon votre position géographique, un pont pourrait fonctionner mieux qu’un autre.">
<!ENTITY torPreferences.bridgeLocation "Votre position">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatique">
<!ENTITY torPreferences.bridgeLocationFrequent "Emplacements sélectionnés fréquemment">
<!ENTITY torPreferences.bridgeLocationOther "Autre emplacements">
<!ENTITY torPreferences.bridgeChooseForMe "Choisir un pont pour moi…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Vos ponts actuels">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "Vous pouvez enregistrer un ou plusieurs ponts et Tor choisira lequel utiliser lors de la connexion. Si besoin est, Tor basculera automatiquement vers un autre pont.">
<!ENTITY torPreferences.bridgeId "Pont #1 : #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Supprimer">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Désactiver les ponts intégrés">
<!ENTITY torPreferences.bridgeShare "Partagez ce pont grâce au code QR ou en copiant son adresse :">
<!ENTITY torPreferences.bridgeCopy "Copier l’adresse du pont">
<!ENTITY torPreferences.copied "A été copié">
<!ENTITY torPreferences.bridgeShowAll "Afficher tous les ponts">
<!ENTITY torPreferences.bridgeRemoveAll "Supprimer tous les ponts">
<!ENTITY torPreferences.bridgeAdd "Ajouter un nouveau pont">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choisir l’un des ponts intégrés du Navigateur Tor">
<!ENTITY torPreferences.bridgeSelectBuiltin "Sélectionner un pont intégré…">
<!ENTITY torPreferences.bridgeRequest "Demander un pont…">
<!ENTITY torPreferences.bridgeEnterKnown "Saisir une adresse de pont que vous connaissez déjà">
<!ENTITY torPreferences.bridgeAddManually "Ajouter un pont manuellement…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Avancé">
<!ENTITY torPreferences.advancedDescription "Configurer la façon dont le Navigateur Tor se connecte à Internet">
<!ENTITY torPreferences.advancedButton "Paramètres…">
<!ENTITY torPreferences.viewTorLogs "Visualiser les journaux de Tor">
<!ENTITY torPreferences.viewLogs "Visualiser les journaux…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Supprimer tous les ponts ?">
<!ENTITY torPreferences.removeBridgesWarning "Cette action ne peut pas être annulée.">
<!ENTITY torPreferences.cancel "Annuler">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Balayer le code QR">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Ponts intégrés">
<!ENTITY torPreferences.builtinBridgeDescription "Le Navigateur Tor comprend des types particuliers de ponts appelés « transports enfichables ».">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 est un type de ponts intégrés qui donne au trafic de Tor une apparence aléatoire. Ils sont aussi moins susceptibles d’être bloqués que leurs prédécesseurs, les ponts obfs3.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake est un pont intégré qui déjoue la censure en acheminant votre connexion par des mandataires Snowflake, exploités par des bénévoles.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure est un pont intégré qui donne l’impression que vous utilisez un site Web de Microsoft au lieu de Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Demander un pont">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Connexion à BridgeDB. Veuillez patienter.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Résolvez le captcha pour demander un pont.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "La solution est erronée. Veuillez réessayer.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Fournir un pont">
<!ENTITY torPreferences.provideBridgeHeader "Saisir des renseignements de pont provenant d’une source fiable">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Paramètres de connexion">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configurer la façon dont le Navigateur Tor se connecte à Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Valeurs séparées par des virgules">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Journaux de Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Non Connecté">
<!ENTITY torConnect.connectingConcise "Connexion…">
<!ENTITY torConnect.tryingAgain "Nouvel essai…">
<!ENTITY torConnect.noInternet "Le Navigateur Tor n’a pu atteindre Internet">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Le Navigateur Tor n’a pas réussi à se connecter à Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configurer votre connexion"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Si Tor est bloqué où vous vous trouvez, un pont pourrait aider. L’assistance à la connexion peut en choisir un pour vous d’après votre position géographique, ou vous pouvez plutôt en #1 manuellement."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Tentative avec un pont…">
<!ENTITY torConnect.tryingBridgeAgain "Encore un autre essai…">
<!ENTITY torConnect.errorLocation "Le Navigateur Tor n’a pas réussi à vous localiser">
<!ENTITY torConnect.errorLocationDescription "Le Navigateur Tor a besoin de connaître votre position géographique afin de choisir le bon pont pour vous. Si vous préférez ne pas partager votre position, #1 plutôt un manuellement."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Ces paramètres de localisation sont-ils bons ?">
<!ENTITY torConnect.isLocationCorrectDescription "Le Navigateur Tor n’a toujours pas réussi à se connecter à Tor. Veuillez vérifier que vos paramètres de localisation sont correctes et réessayer, ou #1 à la place"> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser still cannot connect">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "Assistance à la connexion">
<!ENTITY torConnect.breadcrumbLocation "Paramètres de localisation">
<!ENTITY torConnect.breadcrumbTryBridge "Essayer un pont">
<!ENTITY torConnect.automatic "Automatique">
<!ENTITY torConnect.selectCountryRegion "Sélectionner le pays ou la région">
<!ENTITY torConnect.frequentLocations "Emplacements sélectionnés fréquemment">
<!ENTITY torConnect.otherLocations "Autre emplacements">
<!ENTITY torConnect.restartTorBrowser "Redémarrer le Navigateur Tor">
<!ENTITY torConnect.configureConnection "Configurer la connexion…">
<!ENTITY torConnect.viewLog "Visualiser les journaux…">
<!ENTITY torConnect.tryAgain "Réessayer">
<!ENTITY torConnect.offline "Internet est inaccessible">
<!ENTITY torConnect.connectMessage "Les changements des paramètres de Tor ne seront appliqués qu’après connexion">
<!ENTITY torConnect.tryAgainMessage "Le Navigateur Tor n’a pas réussi à établir une connexion au réseau Tor">
<!ENTITY torConnect.yourLocation "Votre position">
<!ENTITY torConnect.tryBridge "Essayer un pont">
<!ENTITY torConnect.autoBootstrappingFailed "Échec de configuration automatique">
<!ENTITY torConnect.autoBootstrappingFailed "Échec de configuration automatique">
<!ENTITY torConnect.cannotDetermineCountry "Impossible de déterminer le pays de l’utilisateur">
<!ENTITY torConnect.noSettingsForCountry "Aucun paramètre n’est proposé pour votre position">
