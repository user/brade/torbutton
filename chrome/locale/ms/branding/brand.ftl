# For Tor Browser, we use a new file (different than the brand.ftl file
# that is used by Firefox) to avoid picking up the -brand-short-name values
# that Mozilla includes in the Firefox language packs.

-brand-shorter-name = Pelayar Tor
-brand-short-name = Pelayar Tor
-brand-full-name = Pelayar Tor
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Pelayar Tor
-vendor-short-name = Projek Tor
trademarkInfo = 'Tor' dan 'Logo Onion' adalah cap dagangan berdaftar bagi Projek Tor, Inc.
