<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Quant al Tor">

<!ENTITY aboutTor.viewChangelog.label "Mostra el registre de canvis">

<!ENTITY aboutTor.ready.label "Exploreu. Privadament.">
<!ENTITY aboutTor.ready2.label "Esteu preparat per a l'experiència de navegació més privada del món.">
<!ENTITY aboutTor.failure.label "S'ha produït un error.">
<!ENTITY aboutTor.failure2.label "Tor no funciona en aquest navegador.">

<!ENTITY aboutTor.search.label "Cerca amb DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Cap pregunta?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Consulteu el nostre manual del navegador Tor »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Manual del navegador Tor">

<!ENTITY aboutTor.tor_mission.label "El projecte Tor és una US 501(c)(3) organització sense ànim de lucre que avança els drets i les llibertats dels drets humans mitjançant la creació i implementació d'anonimat de codi obert i lliure i tecnologies de privadesa, que donen suport a la seva disponibilitat i ús sense restriccions i fomenten la seva comprensió científica i popular.">
<!ENTITY aboutTor.getInvolved.label "Col·laboreu-hi »">

<!ENTITY aboutTor.newsletter.tagline "Obteniu les darreres novetats de Tor directament a la safata d'entrada.">
<!ENTITY aboutTor.newsletter.link_text "Inscriviu-vos a les noticies de Tor.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor és d'ús gratuït gràcies als donatius de persones com vós.">
<!ENTITY aboutTor.donationBanner.buttonA "Feu una donació">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "FEU UNA DONACIÓ">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "_Friends of Tor_ igualarà la vostra donació fins a 100.000 dòlars.">
