<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "เกี่ยวกับ Tor">

<!ENTITY aboutTor.viewChangelog.label "ดูบันทึกการเปลี่ยนแปลง">

<!ENTITY aboutTor.ready.label "ค้นหาอย่างปลอดภัย">
<!ENTITY aboutTor.ready2.label "คุณพร้อมแล้วสำหรับประสบการณ์การท่องอินเทอร์เน็ตที่ปลอดภัย">
<!ENTITY aboutTor.failure.label "แย่แล้ว มีอะไรบางอย่างผิดพลาด">
<!ENTITY aboutTor.failure2.label "Tor ทำงานกับเบราว์เซอร์นี้ไม่ได้">

<!ENTITY aboutTor.search.label "ค้นหาโดย DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "มีคำถามไหม">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "ดูคู่มือผู้ใช้งาน Tor Browser">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "คู่มือผู้ใช้งาน Tor Browser">

<!ENTITY aboutTor.tor_mission.label "โปรเจค Tor เป็นขององค์กรไม่แสวงหาผลกำไรที่ทำงานเพื่อส่งเสริมสิทธิมนุษยชนและเสรีภาพ โดยการสร้างและบริการเทคโนโลยีที่ฟรี เน้นความเป็นนิรนามและโอเพนซอร์ส และส่งเสริมความเป็นส่วนตัว เพื่อสนับสนุนการใช้งานที่ไม่ต้องถูกจำกัด และสร้างความเข้าใจในสาธารณะให้กว้างขึ้น">
<!ENTITY aboutTor.getInvolved.label "มาร่วมกับเรา">

<!ENTITY aboutTor.newsletter.tagline "ส่งตรงข่าวสารเกี่ยวกับ Tor ไปยังกล่องจดหมายของคุณ">
<!ENTITY aboutTor.newsletter.link_text "สมัครรับข่าวสารเกี่ยวกับ Tor">
<!ENTITY aboutTor.donationBanner.freeToUse "บริการ Tor นี้ฟรี ไม่มีค่าใช้จ่ายใดๆเพราะเงินบริจาคจากผู้มีน้ำใจอย่างคุณ">
<!ENTITY aboutTor.donationBanner.buttonA "ร่วมบริจาค">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "บริจาคตอนนี้">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Your donation will be matched by Friends of Tor, up to $100,000.">
