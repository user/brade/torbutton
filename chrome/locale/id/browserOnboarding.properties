# Copyright (c) 2019, The Tor Project, Inc.
# See LICENSE for licensing information.
# vim: set sw=2 sts=2 ts=8 et:

onboarding.tour-tor-welcome=Selamat datang
onboarding.tour-tor-welcome.title=Anda siap.
onboarding.tour-tor-welcome.description=Tor Browser menawarkan standar tertinggi atas privasi dan keamanan saat menjelajahi jaringan. Anda sekarang dilindungi dari pelacakan, pengintaian, dan penyensoran. Pelatihan singkat ini akan menunjukkan Anda bagaimana caranya.
onboarding.tour-tor-welcome.next-button=Pergi ke Privasi

onboarding.tour-tor-privacy=Privasi
onboarding.tour-tor-privacy.title=Menolak pelacak dan pengintai.
onboarding.tour-tor-privacy.description=Tor Browser mengisolasi cookie dan menghapus riwayat peramban Anda setelah ditutup. Modifikasi ini menjamin privasi dan keamanan telah terproteksi di dalam peramban. Klik 'Jaringan Tor' untuk pempelajari bagaimana kami melindungi Anda pada level jaringan.
onboarding.tour-tor-privacy.button=Pergi ke Jaringan Tor

onboarding.tour-tor-network=Jaringan Tor
onboarding.tour-tor-network.title=Melintasi jaringan yang terdesentralisasi.
onboarding.tour-tor-network.description=Tor Browser menghubungkan Anda ke jaringan Tor yang dijalankan oleh ribuan relawan di seluruh dunia. Tidak seperti VPN, tidak ada titik tunggal kegagalan atau entitas sentral yang perlu Anda percaya untuk menikmati Internet secara privat.
onboarding.tour-tor-network.description-para2=BARU: Pengaturan Jaringan Tor, termasuk kemampuan untuk meminta bridge dimana Tor diblokir, sekarang dapat ditemukan di Preferensi.
onboarding.tour-tor-network.action-button=Sesuaikan Pengaturan Jaringan Tor Anda
onboarding.tour-tor-network.button=Pergi ke Tampilan Sirkuit

onboarding.tour-tor-circuit-display=Tampilan Sirkuit
onboarding.tour-tor-circuit-display.title=Lihat jalur Anda.
onboarding.tour-tor-circuit-display.description=Untuk setiap domain yang Anda kunjungi, lalu lintas Anda disampaikan dan terenkripsi dalam sebuah sirkuit melalui tiga relay Tor dari seluruh dunia. Tidak ada situs web mengetahui dari mana Anda terhubung. Anda dapat meminta sebuah sirkuit baru dengan klik 'Sirkuit Baru untuk Situs ini' pada Tampilan Sirkuit kami.
onboarding.tour-tor-circuit-display.button=Lihat Jalur Saya
onboarding.tour-tor-circuit-display.next-button=Pergi ke Keamanan

onboarding.tour-tor-security=Keamanan
onboarding.tour-tor-security.title=Pilih pengalaman Anda.
onboarding.tour-tor-security.description=Kami juga memberi Anda pengaturan tambahan untuk menggenjot keamanan peramban Anda. Pengaturan Keamanan kami mengizinkan Anda untuk memblokir elemen-elemen yang dapat digunakan untuk menyerang komputer Anda. Klik di bawah untuk mengetahui apa saja yang dilakukan oleh pilihan yang berbeda.
onboarding.tour-tor-security.description-suffix=Catatan: Secara bawaan, NoScript dan HTTPS Everywhere tidak termasuk di dalam bilah alat, namun Anda dapat menyesuaikan bilah alat Anda untuk menambahkannya.
onboarding.tour-tor-security-level.button=Lihat Level Keamanan Anda
onboarding.tour-tor-security-level.next-button=Pergi ke Tips Pengalaman

onboarding.tour-tor-expect-differences=Tips Pengalaman
onboarding.tour-tor-expect-differences.title=Harapkan beberapa perubahan.
onboarding.tour-tor-expect-differences.description=Dengan semua fitur keamanan dan privasi yang disediakan oleh Tor, pengalaman Anda saat menjelajahi internet mungkin akan sedikit berbeda. Kecepatan internet mungkin akan berkurang, dan berdasarkan level keamanan Anda, beberapa elemen mungkin tidak bisa bekerja atau dimuat. Anda mungkin juga akan mendapatkan pertanyaan untuk membuktikan bahwa Anda adalah manusia dan bukan robot.
onboarding.tour-tor-expect-differences.button=Lihat FAQ
onboarding.tour-tor-expect-differences.next-button=Pergi ke Layanan Onion

onboarding.tour-tor-onion-services=Layanan Onion
onboarding.tour-tor-onion-services.title=Menjadi lebih terlindung.
onboarding.tour-tor-onion-services.description=Layanan Onion adalah situs yang berakhir dengan .onion yang menyediakan perlindungan lebih kepada penerbit dan pengunjung, termasuk tambahan penjagaan terhadap sensor. Layanan Onion mengizinkan siapa saja untuk menyediakan konten dan layanan secara anonim. Klik di bawah untuk mengunjungi situs onion DuckDuckGo.
onboarding.tour-tor-onion-services.button=Kunjungi sebuah Onion
onboarding.tour-tor-onion-services.next-button=Selesai

onboarding.overlay-icon-tooltip-updated2=Lihat apa yang baru\ndi %S
onboarding.tour-tor-update.prefix-new=Baru
onboarding.tour-tor-update.prefix-updated=Telah diperbarui

onboarding.tour-tor-toolbar=Bilah Alat
onboarding.tour-tor-toolbar-update-9.0.title=Selamat tinggal Onion Button.
onboarding.tour-tor-toolbar-update-9.0.description=Kami menginginkan pengalaman Anda menggunakan Tor terintegrasi secara penuh dalam Tor Browser.
onboarding.tour-tor-toolbar-update-9.0.description-para2=Itulah sebabnya sekarang, daripada mengunakan tombol onion, kamu dapat melihat sirkuit Tor lewat [i] di bilah URL dan meminta sebuah Identitas Baru menggunakan tombol bilah alat atau menu [≡].
onboarding.tour-tor-toolbar-update-9.0.button=Bagaimana Meminta Identitas Baru
onboarding.tour-tor-toolbar-update-9.0.next-button=Pergi ke Jaringan Tor

# Circuit Display onboarding.
onboarding.tor-circuit-display.next=Selanjutnya
onboarding.tor-circuit-display.done=Selesai
onboarding.tor-circuit-display.one-of-three=1 dari 3
onboarding.tor-circuit-display.two-of-three=2 dari 3
onboarding.tor-circuit-display.three-of-three=3 dari 3

onboarding.tor-circuit-display.intro.title=Bagaimana sirkuit bekerja?
onboarding.tor-circuit-display.intro.msg=Sirkuit terbuat dari relay yang ditentukan secara acak, yang merupakan komputer di seluruh dunia yang terkonfigurasi untuk mengalihkan lalu lintas Tor. Sirkuit mengizinkan anda untuk menjelajah secara privat dan terhubung ke layanan onion.

onboarding.tor-circuit-display.diagram.title=Tampilan Sirkuit
onboarding.tor-circuit-display.diagram.msg=Diagram ini menunjukkan relay-relay yang membuat sirkuit untuk situs web ini. Untuk mencegah pengkaitan antara aktivitas pada situs berbeda, setiap situs web mendapatkan sirkuit yang berbeda.

onboarding.tor-circuit-display.new-circuit.title=Apakah Anda menginginkan sirkuit baru?
onboarding.tor-circuit-display.new-circuit.msg=Bila Anda tidak berhasil terhubung ke situs web yang Anda coba kunjungi atau situs web tersebut tidak memuat dengan benar, maka Anda bisa menggunakan tombol ini untuk membuka ulang situs tersebut dengan sirkuit baru.
